import { Component, OnInit } from '@angular/core';
import products from '../files/products.json';
import * as _ from 'underscore';

@Component({
  selector: 'app-pos',
  templateUrl: './pos.component.html',
  styleUrls: ['./pos.component.scss'],
})
export class PosComponent implements OnInit {
  productList: any = [];
  addItemList: any = [];
  subTotal: any = 0;
  vatTax: any = 0;
  discount: any = 0;
  count: any = 0;
  total: any;

  isItem: boolean = false;
  constructor() {}

  ngOnInit(): void {
    this.productList = products;
    localStorage.setItem('list', JSON.stringify(this.productList));
    console.log(products);
  }

  async addItem(index: number) {
    this.count++;

    this.productList[index]['qty']++;
    this.productList[index]['totol'] =
      this.productList[index]['price'] * this.productList[index]['qty'];
    this.subTotal += parseInt(this.productList[index]['price']);
    this.discount = this.subTotal / 10;
    this.vatTax = this.subTotal / 10;
    this.total = this.subTotal + this.vatTax + this.discount;
  }
  handleValue(key: any, i: any, type: any) {
    if (type == 'inr') {
      this.count++;
      this.productList[i][key]++;
      this.productList[i]['totol'] =
        this.productList[i]['price'] * this.productList[i][key];
    } else {
      this.count--;

      if (this.productList[i][key] > 0) {
        this.productList[i][key]--;
      }
    }
    this.subTotal += parseInt(this.productList[i]['price']);

    this.discount = this.subTotal / 10;
    this.vatTax = this.subTotal / 10;
    this.total = this.subTotal + this.vatTax + this.discount;
  }
  delete(i: any) {
    this.count -= this.productList[i]['qty'];
    this.subTotal -= this.productList[i]['totol'];
    this.discount = this.subTotal / 10;
    this.vatTax = this.subTotal / 10;
    this.total = this.subTotal + this.vatTax + this.discount;
    this.productList[i]['totol'] = 0;
    this.productList[i]['qty'] = 0;
  }
  async cancel() {
    const res: any = await localStorage.getItem('list');
    this.productList = JSON.parse(res);
    this.subTotal = 0;
    this.discount = 0;
    this.vatTax = 0;
    this.total = 0;
    this.count = 0;
  }
}
